---
title: "Powered by the SIS-CC .Stat Suite"
subtitle: 
comments: false
weight: 14

---

List of organisations or initiatives platforms built with and powered by the .Stat Suite:

---

![Australian Bureau of Statistics (ABS)](/dotstatsuite-documentation/images/logo-abs.png)

**Australian Bureau of Statistics (ABS)** [Data Explorer](https://www.abs.gov.au/about/data-services/explore/data-explorer)

---

![National Statistical Institute of Cambodia](/dotstatsuite-documentation/images/logo-nis.png)

**National Statistical Institute of Cambodia** [CamStat](http://camstat.nis.gov.kh/)

---

![Ministry of Labour and Social Welfare of El Salvador (MTPS)](/dotstatsuite-documentation/images/logo_SIMEL.Stat.png)

**Ministry of Labour and Social Welfare of El Salvador (MTPS)** [SIMEL.Stat](https://datasimel.mtps.gob.sv/)

---

![Federal Competitiveness and Statistics Centre (FCSC)](/dotstatsuite-documentation/images/logo-fcsc.png)

**Federal Competitiveness and Statistics Centre (FCSC)** [UAE.Stat](https://uaestat.fcsc.gov.ae/en)

---

![National Institute of Statistics and Economic Studies of the Grand Duchy of Luxembourg (STATEC)](/dotstatsuite-documentation/images/logo-statec.png)

**STATEC** [Data Explorer](https://lustat.statec.lu/)

---

![Pacific Community](/dotstatsuite-documentation/images/logo-pc.png)

**Pacific Community** [Pacific Data Hub](https://stats.pacificdata.org/#/)

---

![UNICEF](/dotstatsuite-documentation/images/logo-unicef.png)

**UNICEF** [Data Warehouse](https://data.unicef.org/dv_index/)

---

![UNESCAP SDG Gateway Data](/dotstatsuite-documentation/images/logo-unescap.png)

**UNESCAP** [SDG Gateway Data](https://dataexplorer.unescap.org/)

---

![United Nations Inter-agency Group](/dotstatsuite-documentation/images/logo-igme.png)

**United Nations Inter-agency Group** [Child Mortality Estimation](https://childmortality.org/)

---
